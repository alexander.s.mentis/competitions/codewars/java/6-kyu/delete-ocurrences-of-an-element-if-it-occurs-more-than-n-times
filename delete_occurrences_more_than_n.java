import java.util.ArrayList;
import java.util.HashMap;

public class EnoughIsEnough {

	public static int[] deleteNth(int[] elements, int maxOccurrences) {
		
    HashMap<Integer, Integer> counts = new HashMap<>();
    ArrayList<Integer> result = new ArrayList<>(elements.length);
    
    for (int e : elements) {
      counts.put(e, counts.getOrDefault(e, 0) + 1);
      if (counts.get(e) <= maxOccurrences) {
        result.add(e);
      }
    }
    
    return result.stream().mapToInt(i -> i).toArray();
	}

}